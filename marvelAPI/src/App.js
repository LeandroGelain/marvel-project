import React from 'react';
import './App.css';

import Characters from './components/characters'
import {Grid, Typography, Box} from '@material-ui/core'
function App() {
  return (
    <>
      <Grid container alignContent='center' justify >
        <Grid item direction='row' xs={12}>
          <Box m={2} className="title-align">
            <Typography variant='h4' className="title">
              Personagens
            </Typography>
            <img style={{width: '100px', paddingTop:'10px'}} src="https://upload.wikimedia.org/wikipedia/commons/0/04/MarvelLogo.svg"/>
          </Box>
        </Grid>
      </Grid>
      <Characters/>
    </>
  );
}

export default App;
